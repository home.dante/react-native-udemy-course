import {Pressable, StyleProp, StyleSheet, Text, TextStyle, View} from 'react-native';
import React from 'react';
import {Ionicons} from '@expo/vector-icons';
export type IconButton = {
    // Define your props here
    onPressHandler?: () => void,
    icon: keyof typeof Ionicons.glyphMap;
    color?: string,
    size?: number,
    iconStyle?: StyleProp<TextStyle>,
    style?: StyleProp<TextStyle>
};

const IconButton: React.FC<IconButton> = ({icon, color="white", size=24, onPressHandler, iconStyle, style}) => {
    return (
        <Pressable 
        android_ripple={{color: '#ccc',  borderless:true}}
        style={({pressed}) => [styles.button,style, pressed ? styles.buttonPressed : null]}
        onPress={onPressHandler}>
            <Ionicons style={[styles.iconStyle, iconStyle]} name={icon} size={size} color={color}></Ionicons>
        </Pressable>
    );
};

export default IconButton;

const styles = StyleSheet.create({
    button: {
    overflow:"hidden"
    },
    buttonPressed:{
    },
    iconStyle:{padding:5}
});
