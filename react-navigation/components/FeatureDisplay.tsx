import React from 'react';
import {StyleSheet, Text} from 'react-native';

export type FeatureDisplayProps = {
    label: string;
    value: boolean | number | string;
};

const FeatureDisplay: React.FC<FeatureDisplayProps> = ({label, value}) => {
    return (
        <Text style={styles.details}>
            {label}: {value ? 'Yes' : 'No'}
        </Text>
    );
};

export default FeatureDisplay;

const styles = StyleSheet.create({
    details: {
        fontSize: 16,
        marginVertical: 2,
        textAlign: 'center'
    }
});
