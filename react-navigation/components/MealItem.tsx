import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity} from 'react-native';
import Meal from '../models/meal';

export type MealItemProps = {
    meal: Meal;
    onPressHandler: (id: string) => void;
};

export const MealItem: React.FC<MealItemProps> = ({meal, onPressHandler}) => {
    const onMealPressHandler = () => {
        // console.log(meal.id)
        onPressHandler(meal.id);
    };

    return (
        <TouchableOpacity onPress={onMealPressHandler} key={meal.id} style={styles.mealContainer}>
            <Image source={{uri: meal.imageUrl}} style={styles.image} />
            <Text style={styles.mealTitle}>{meal.title}</Text>
            <Text style={styles.details}>Duration: {meal.duration} mins</Text>
            <Text style={styles.details}>Complexity: {meal.complexity}</Text>
            <Text style={styles.details}>Affordability: {meal.affordability}</Text>
        </TouchableOpacity>
    );
};

export const styles = StyleSheet.create({
    details: {
        fontSize: 16,
        marginVertical: 2,
        textAlign: 'center'
    },
    image: {
        borderRadius: 10,
        height: 200,
        width: '100%'
    },
    mealContainer: {
        backgroundColor: 'white',
        borderRadius: 10,
        elevation: 5,
        marginBottom: 20,
        padding: 10,
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.1,
        shadowRadius: 8
    },
    mealTitle: {
        fontSize: 24,
        fontWeight: 'bold',
        marginVertical: 10,
        textAlign: 'center'
    }
});
