import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

export type ListDisplayProps<T> = {
    title: string;
    items: Array<T>;
};

const ListDisplay = <T extends string>({title, items}: ListDisplayProps<T>) => {
    return (
        <View>
            <Text style={styles.subTitle}>{title}</Text>
            {items.map((item, index) => (
                <Text key={index} style={styles.item}>
                    {title === 'Steps:' ? `${index + 1}. ` : '- '}
                    {item}
                </Text>
            ))}
        </View>
    );
};

export default ListDisplay;

const styles = StyleSheet.create({
    item: {
        fontSize: 16,
        marginVertical: 5
    },
    subTitle: {
        fontSize: 20,
        fontWeight: 'bold',
        marginBottom: 5,
        marginTop: 10
    }
});
