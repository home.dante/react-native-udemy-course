import React, { useContext, useEffect, useLayoutEffect, useState } from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import {MealItem} from '../components/MealItem';
import {MEALS} from '../data/dummy-data';
import {ScreenProps} from '../models/ScreenProps';
import { FavoritesContext } from '../store/context/favorites-context';

const FavoritesScreen: React.FC<ScreenProps<'FavoritesScreen'>> = ({route, navigation}) => {
  // const {id, title, color} = route.params;
  const ctx = useContext(FavoritesContext);
  const [favoriteMeals, setFavoriteMeals] = useState(getFavoritesArray(ctx.ids));

  useEffect(()=>{
    setFavoriteMeals(()=>getFavoritesArray(ctx.ids));
  },[ctx.ids]);

  const onHandlerName = (mealId: string) => {
      navigation.navigate('MealDetailsScreen', {id: mealId, color:"transparent"});
  };

  if(favoriteMeals.length === 0) {
    return <View style={{flex:1, justifyContent:"center", alignItems:"center", padding:32}}>
      <Text style={{color:"white", fontSize:26, textAlign:"center"}}>
        You have no favorite meals yet.
      </Text>
    </View>
  }

  return (
      <View style={styles.container}>
          <FlatList data={favoriteMeals} renderItem={({item}) => <MealItem onPressHandler={onHandlerName} meal={item}></MealItem>} keyExtractor={(meal) => meal.id}></FlatList>
      </View>
  );


  function getFavoritesArray(favoritesIds:string[]) {
    return MEALS.filter((meal) => favoritesIds.includes(meal.id));
  }
};




const styles = StyleSheet.create({
  container: {
      flex: 1,
      padding: 16
  },
});

export default FavoritesScreen;