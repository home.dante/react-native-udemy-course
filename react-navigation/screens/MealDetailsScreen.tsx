import React, { useContext, useLayoutEffect, useState } from 'react';
import { Image, ScrollView, StyleSheet, Text, View } from 'react-native';
import FeatureDisplay from '../components/FeatureDisplay';
import IconButton from '../components/IconButton';
import ListDisplay from '../components/ListDisplay';
import { MEALS } from '../data/dummy-data';
import { ScreenProps } from '../models/ScreenProps';
import Meal from '../models/meal';
import { FavoritesContext } from '../store/context/favorites-context';

const MealDetailsScreen: React.FC<ScreenProps<'MealDetailsScreen'>> = ({route, navigation}) => {
    const {id, color} = route.params;
    const [meal, setMeal] = useState<Meal | null>(null);
    const ctx = useContext(FavoritesContext);
    const isFavorite = ctx.ids.includes(id);

    const onButtonPressHandler = () => {
        isFavorite ? ctx.removeFavorite(id) : ctx.addFavorite(id);
    };

    useLayoutEffect(() => {
        // // console.log("meals are", MEALS);
        const foundMeal = MEALS.find((meal: Meal) => meal.id === id);
        setMeal(foundMeal || null);
        navigation.setOptions({
            title: foundMeal.title,
            headerRight: () => {
                return <IconButton color="white" size={22} icon={isFavorite ? 'star' : 'star-outline'} onPressHandler={onButtonPressHandler}></IconButton>;
            }
        });
    }, [id, onButtonPressHandler]);

    if (!meal) {
        return (
            <View>
                <Text>No meal found!</Text>
            </View>
        );
    }

    return (
        <View style={{flex: 1, backgroundColor: color || 'grey', padding: 16}}>
            <ScrollView style={{flex: 1}}>
                <View style={[styles.mealContainer, {}]}>
                    <Image source={{uri: meal.imageUrl}} style={styles.image} />
                    <Text style={styles.mealTitle}>{meal.title}</Text>
                    <Text style={styles.details}>Duration: {meal.duration} mins</Text>
                    <Text style={styles.details}>Complexity: {meal.complexity}</Text>
                    <Text style={styles.details}>Affordability: {meal.affordability}</Text>
                    <FeatureDisplay label="Gluten Free" value={meal.isGlutenFree} />
                    <FeatureDisplay label="Vegan" value={meal.isVegan} />
                    <FeatureDisplay label="Vegetarian" value={meal.isVegetarian} />
                    <FeatureDisplay label="Lactose Free" value={meal.isLactoseFree} />
                    <ListDisplay title="Ingredients:" items={meal.ingredients} />
                    <ListDisplay title="Steps:" items={meal.steps} />
                </View>
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create({
    details: {
        fontSize: 16,
        marginVertical: 2,
        textAlign: 'center'
    },
    image: {
        borderRadius: 10,
        height: 200,
        width: '100%'
    },
    mealContainer: {
        backgroundColor: 'white',
        borderRadius: 10,
        elevation: 5,
        marginBottom: 20,
        padding: 16,
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.1,
        shadowRadius: 8
    },
    mealTitle: {
        fontSize: 24,
        fontWeight: 'bold',
        marginVertical: 10,
        textAlign: 'center'
    }
});

export default MealDetailsScreen;
