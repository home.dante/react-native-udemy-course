import React, {useEffect, useState} from 'react';
import {FlatList, StyleSheet} from 'react-native';
import CategoryGridTile, {CategoryGridTileProps} from '../components/CategoryGridTile';
import {CATEGORIES} from '../data/dummy-data';
import {MealsOverviewRouteParams} from '../models/RootStackParamList';
import {ScreenProps} from '../models/ScreenProps';

const CategoriesScreen: React.FC<ScreenProps<'CategoriesScreen'>> = ({route, navigation}) => {
    const [categories, setCategories] = useState(CATEGORIES);

    const onCategoryPressHandler = ({title, color, id}: MealsOverviewRouteParams) => {
        navigation.navigate('MealsOverviewScreen', {id, title, color});
    };

    useEffect(() => {
        console.log(CATEGORIES);
    });

    const renderCategory = ({onCategoryPressHandler, title, color, id}: CategoryGridTileProps) => {
        return <CategoryGridTile onCategoryPressHandler={onCategoryPressHandler} title={title} color={color} id={id}></CategoryGridTile>;
    };
    return <FlatList scrollEnabled={true} numColumns={2} data={categories} renderItem={({item}) => renderCategory({onCategoryPressHandler, ...item})} keyExtractor={(item) => item.id} />;
};

export default CategoriesScreen;

const styles = StyleSheet.create({});
