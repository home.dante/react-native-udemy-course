import React, { useEffect, useLayoutEffect, useState } from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import {MealItem} from '../components/MealItem';
import {MEALS} from '../data/dummy-data';
import {ScreenProps} from '../models/ScreenProps';

const MealsOverviewScreen: React.FC<ScreenProps<'MealsOverviewScreen'>> = ({route, navigation}) => {
    const {id, title, color} = route.params;
    const [meals, setMeals] = useState(MEALS.filter((meal) => meal.categoryIds.includes(id)));

    useLayoutEffect(()=>{
      navigation.setOptions({title:title});
    },[title]);

    const onHandlerName = (mealId: string) => {
        navigation.navigate('MealDetailsScreen', {id: mealId, color});
    };

    return (
        <View style={styles.container}>
            <FlatList data={meals} renderItem={({item}) => <MealItem onPressHandler={onHandlerName} meal={item}></MealItem>} keyExtractor={(meal) => meal.id}></FlatList>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 16
    },
});

export default MealsOverviewScreen;
