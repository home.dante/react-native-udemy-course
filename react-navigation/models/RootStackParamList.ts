export type RootStackParamList = {
  MealsOverviewScreen: MealsOverviewRouteParams;
  CategoriesScreen: CategoriesScreenRouteParams;
  MealDetailsScreen: MealDetailsScreenRouteParams;
  FavoritesScreen: FavoritesScreenRouteParams;
  DrawerScreen: DrawerScreenRouteParams;
  // Add other screens here if needed
};

export type MealsOverviewRouteParams = {
  id: string;
  title: string;
  color: string;
};

export type CategoriesScreenRouteParams = {
  [key: string]: unknown;
};

export type MealDetailsScreenRouteParams = {
  id: string;
  color: string;
};

export type FavoritesScreenRouteParams = {
  [key: string]: unknown;
};
export type DrawerScreenRouteParams = {
  [key: string]: unknown;
};
