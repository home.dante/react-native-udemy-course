import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';
import { RootStackParamList } from './models/RootStackParamList';
import CategoriesScreen from './screens/CategoriesScreen';
import MealDetailsScreen from './screens/MealDetailsScreen';
import MealsOverviewScreen from './screens/MealsOverviewScreen';
import { createDrawerNavigator } from '@react-navigation/drawer';
import FavoritesScreen from './screens/FavoritesScreen';
import 'react-native-gesture-handler';
import {Ionicons} from '@expo/vector-icons';
import {FavoritesContextProvider} from './store/context/favorites-context';
const Stack = createNativeStackNavigator<RootStackParamList>();
const Drawer = createDrawerNavigator<RootStackParamList>()

function DrawerNavigator() {
return <Drawer.Navigator
screenOptions={() => {
    return {
        drawerActiveTintColor:"white",
        drawerActiveBackgroundColor:'#4d362c',
        drawerContentStyle:{backgroundColor:"#351101"},
        // drawerInactiveBackgroundColor: "transparent",
        drawerInactiveTintColor:'gray',
        headerStyle: {
            backgroundColor: '#351101'
        },
        headerTintColor: 'white',
        sceneContainerStyle: {backgroundColor: '#4d362c'}
    };
}}
>
    <Drawer.Screen 
    options={{
        headerTitleStyle:{fontSize:22},
        drawerIcon:(({color,size})=><Ionicons size={size}  name='star' color={color}></Ionicons>),
        title:"Favorites"}}
    name="FavoritesScreen" component={FavoritesScreen}  />    
    <Drawer.Screen name="CategoriesScreen" component={CategoriesScreen}
    options={{
        headerTitleStyle:{fontSize:22},
        drawerIcon:(({color,size})=><Ionicons size={size}  name='list' color={color}></Ionicons>),
        title:"All Categories"}}
    />    
</Drawer.Navigator>
}

export default function App() {
    return (
        <>
            <StatusBar style="light"></StatusBar>
            <FavoritesContextProvider>
            <NavigationContainer>
                <Stack.Navigator
                    screenOptions={() => {
                        return {
                            headerStyle: {
                                backgroundColor: '#351101'
                            },
                            headerTintColor: 'white',
                            contentStyle: {backgroundColor: '#4d362c'}
                        };
                    }}>
                    <Stack.Screen 
                    options={{headerShown:false}}
                     name="DrawerScreen" component={DrawerNavigator} />
                    <Stack.Screen
                        name="MealsOverviewScreen"
                        component={MealsOverviewScreen}
                    />
                    <Stack.Screen
                        name="MealDetailsScreen"
                        component={MealDetailsScreen}
                        options={{ 
                            
                        }}
                    />
                </Stack.Navigator>
            </NavigationContainer>
            </FavoritesContextProvider>

        </>
    );
}

const styles = StyleSheet.create({});
