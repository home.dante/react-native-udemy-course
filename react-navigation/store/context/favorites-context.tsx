import React, { createContext, useState, ReactNode } from "react";

// Define the shape of your context
interface FavoritesContextType {
  ids: string[];
  addFavorite: (id: string) => void;
  removeFavorite: (id: string) => void;
}

// Create the context with a default value
const FavoritesContext = createContext<FavoritesContextType>({
  ids: [],
  addFavorite: () => {},
  removeFavorite: () => {},
});

// Props type for the provider component
interface FavoritesContextProviderProps {
  children: ReactNode;
}

function FavoritesContextProvider({ children }: FavoritesContextProviderProps) {

  const [favoriteIds, setFavoriteIds] = useState<string[]>([]);

  const addFavorite = (id: string) => {
    setFavoriteIds((currentIds) => [...currentIds, id]);
  };

  const removeFavorite = (id: string) => {
    setFavoriteIds((currentIds) => currentIds.filter((favId) => favId !== id));
  };

  return (
    <FavoritesContext.Provider value={{ids:favoriteIds, addFavorite, removeFavorite}}>
      {children }
    </FavoritesContext.Provider>
  );
}

export {FavoritesContext,FavoritesContextProvider};