import {View, Text, StyleSheet, ImageBackground} from 'react-native';

function WelcomeScreen() {
    return (
        <View style={styles.rootContainer}>
            <Text>
        This is the <Text style={styles.highlight}>"Welcome"</Text> screen!
      </Text>
            {/* <ImageBackground
                imageStyle={styles.image}
                source={require('../assets/background.jpg')}
                style={styles.container}
            /> */}
        </View>
    );
}

export default WelcomeScreen;

const styles = StyleSheet.create({
    rootContainer: {
        flex: 1,

    },
    container: {
    //   resizeMode:"cover",
      flex: 1,
    //   marginLeft:200,
    //   width:"100%",
    },
    image: {
      resizeMode: 'cover',
      width:"100%",
 
    },
    highlight: {
        fontWeight: 'bold',
        color: '#eb1064'
    }
});
