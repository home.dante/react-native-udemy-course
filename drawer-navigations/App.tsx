
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import { StyleSheet } from 'react-native';
import DrawerCustomContent from './DrawerCustomContent';
import UserScreen from './screens/UserScreen';
import WelcomeScreen from './screens/WelcomeScreen';

const Drawer = createDrawerNavigator();

export default function App() {
    return (
        <NavigationContainer>
            <Drawer.Navigator drawerContent={(props) => <DrawerCustomContent {...props} />}>
                <Drawer.Screen
                    options={{
                        headerStyle: {
                            backgroundColor: '#421172'
                        },
                        headerTintColor: 'white',
                        drawerLabel: 'Whale Cum',
                        drawerActiveBackgroundColor: '#6120a1',
                        drawerActiveTintColor: 'white',
                        drawerInactiveBackgroundColor: 'transparent',
                        drawerInactiveTintColor: 'white',
                        drawerStyle:{backgroundColor:"#ccc"}
                    }}
                    name="Welcum"
                    component={WelcomeScreen}
                />
                <Drawer.Screen
                    options={{
                        headerStyle: {
                            backgroundColor: '#421172'
                        },
                        headerTintColor: 'white',
                        drawerLabel: 'Whale Cum',
                        drawerActiveBackgroundColor: '#6120a1',
                        drawerActiveTintColor: 'white',
                        drawerInactiveBackgroundColor: 'transparent',
                        drawerInactiveTintColor: 'white',
                    }}
                    name="User Screen"
                    component={UserScreen}
                />
            </Drawer.Navigator>
        </NavigationContainer>
    );
}

const styles = StyleSheet.create({

});
