/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import React from 'react';
import {DrawerContentScrollView, DrawerItemList} from '@react-navigation/drawer';
import {Image, ImageBackground, StyleSheet, Text, View} from 'react-native';

const DrawerCustomContent = (props: any) => {
    const resizeMode = 'center';
    const text = 'I am some centered text';

    return (
        <DrawerContentScrollView {...props} contentContainerStyle={{flex: 1}}>
            <View style={styles.container}>
                {/* <ImageBackground
                    source={require('./assets/background.jpg')}
                    style={styles.background}
                >
                    <View style={styles.overlay}>
                        <DrawerItemList {...props} />
                    </View>
                </ImageBackground> */}
                <View
                    style={{
                        flex: 1,
                        position: 'relative',
                        backgroundColor: 'transparent',
                        justifyContent: 'flex-start'
                    }}>
                    {/* <View
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            bottom:0,
            right:0

          }}
        >
          <Image
            style={{
              resizeMode:"cover",
                marginLeft:-500
            }}
            source={require('./assets/background.jpg')}
          />
        </View> */}
                    <View
                        style={{
                          position: 'absolute',
                          bottom: 0,
                          top: 0,
                          right:0
                        }}>
                        <Image
                            style={{
                                resizeMode: 'cover',
                                position:"relative",
                                transform: [
                                    {scale:0.70},
                                    {translateX: 1400}, // Adjust based on your image size
                                    {translateY: -300} // Adjust based on your image size
                                ]
                            }}
                            source={require('./assets/background.jpg')}
                        />
                    </View>
                    <View style={styles.overlay}>
                        <DrawerItemList {...props} />
                    </View>
                </View>
            </View>
        </DrawerContentScrollView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1
        // justifyContent:"center",
        // alignItems:"center"
    },
    background: {
        flex: 1,
        justifyContent: 'flex-start', // Align image to the top
        alignItems: 'flex-start', // Align image to the left
        width: null,
        height: null
    },
    overlay: {
        flex: 1,
        backgroundColor: 'rgba(0, 0, 0, 0.5)', // Optional: add an overlay effect
        padding: 20
    }
});

export default DrawerCustomContent;
