import { FlatList, ScrollView, StyleSheet, Text, View } from "react-native";
import { LgbtAvatarComponent } from "./LgbtAvatarComponent";
import { useEffect, useState } from "react";
import LinearGradient from "react-native-linear-gradient";
import Icon from 'react-native-vector-icons/FontAwesome';
import { Button } from "react-native-elements";
import { TextInput } from 'react-native-paper';
import { NavigationContainer } from '@react-navigation/native';
import MainComponent from "./MainComponent";
import {createNativeStackNavigator} from "@react-navigation/native-stack";
import Constants from "expo-constants";
import { Dimensions } from "react-native";
export default function App() {
  const marginTopElement = props => <View style={{marginTop: 0}}></View>;
  const [enteredGoalText, setEnteredGoalText] = useState("")
  const [goalList, setGoalList] = useState([]);
  const Stack = createNativeStackNavigator();
  const [initialRouteName, setInitialRouteName] = useState('Main');
//   useEffect(()=>{
//     setInterval(()=>{
//         const height = Dimensions?.get('window')?.height;
//         console.log(height)
//     }, 3000)
// },[])
  const onAddGoalsHandler = () => {
    // if(enteredGoalText.length < 1){
    //   return;
    // }
    setGoalList((list) => [...list, enteredGoalText])
    setEnteredGoalText("")
  }

  const onChangeTextHandler = (value) => {
    setEnteredGoalText(value)
  }
//   useEffect(() => {
//     if (!isInitialized) {
//         if (principal) {
//             setInitialRouteName('Home');
//         }
//         setIsInitialized(true);
//     }
// }, [principal]);
  return (
  <MainComponent>

  </MainComponent>
    // <NavigationContainer>
    // <Stack.Navigator initialRouteName="Main" detachInactiveScreens={false} screenOptions={{animation: 'none'}}>
    // <Stack.Screen name="Main" component={MainComponent} options={{header: marginTopElement}}/>
    // </Stack.Navigator>

    // </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  appContainer: {
    paddingTop: 50,
    paddingHorizontal: 16,
    flex: 1
  },
  inputContainer: {
    height:100,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 24,
    borderBottomWidth: 1,
    borderBottomColor: "#cccccc"
  },
  textInput: {
    borderWidth: 1,
    borderColor: "#cccccc",
    width: "70%",
    marginRight: 8,
    padding: 8,
    height:20
  },
  button: {
    // justifyContent:"center",
    // alignItems:"center",
    // alignContent:"center"
    backgroundColor: "#E91E63"
  },
  goalsContainer: {
    flex: 5, paddingBottom:20
  }

});
