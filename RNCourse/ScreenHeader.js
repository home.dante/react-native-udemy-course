import {View, StyleSheet, ActivityIndicator} from "react-native";
import {Button, Text, useTheme} from "react-native-paper";
import {FontAwesome} from "@expo/vector-icons";
import {useState} from "react";

/**
 * 
 * @param navigation
 * @param title
 * @param showBackButton
 * @param buttonLabel
 * @param onSubmit - if Observable is provided, the loading bar will be displayed, and no other requests can be made until it is finished. I recommend to pass observable where possible.
 * @param beforeBack
 * @returns {JSX.Element}
 * @constructor
 */
export default function ScreenHeader({navigation, onBack, title, showBackButton = true, buttonLabel, onSubmit, beforeBack, customActionsTemplate}) {
    let theme = useTheme();
    let [loading, setLoading] = useState(false);

    const goBack = () => {
        if (navigation) {
            if (!beforeBack || beforeBack()) {
                navigation.goBack();
            }
        } else {
            onBack?.();
        }

    }

    const submit = () => {
        if (loading) {
            return;
        }
        setLoading(true);
        let submitPromise = onSubmit();
        if (submitPromise?.then) {
            submitPromise.then(() => setLoading(false));
        } else {
            setLoading(false);
        }
    }

    return (
        <View style={{...styles.container, backgroundColor: '#0c1b34'}}>
            <View style={{flexDirection: 'row'}}>
                {showBackButton && <FontAwesome name="chevron-left" size={20} style={styles.backIcon} onPress={goBack}/>}
                <Text style={styles.text}>{title}</Text>
            </View>
            {customActionsTemplate}
            {!customActionsTemplate && onSubmit && (
                loading ? <ActivityIndicator animating={true} color='#fff' /> : <Button style={styles.button} onPress={submit} labelStyle={styles.button} mode='flat'>{buttonLabel || 'Save'}</Button>)}
        </View>
    )

}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        padding: 8,
        paddingTop: 12,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    backIcon: {
        color: 'white',
        padding: 4,
        marginRight: 28
    },
    text: {
        color: 'white',
        fontSize: 18
    },
    button: {
        color: 'white',
        fontSize: 16
    }
});
