import React, { useEffect } from 'react';
import { StyleSheet, View, Dimensions, Image } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Animated, { useAnimatedStyle, useSharedValue, withRepeat, withTiming } from 'react-native-reanimated';

const {width} = Dimensions.get('window');
const ROTATION_TIME = 10000; // 10s

export function LgbtAvatarComponent() {
  const rotation = useSharedValue(0); // Initial rotation angle

  useEffect(() => {
    rotation.value = withRepeat(withTiming(360, {duration: ROTATION_TIME}), -1, true);
  }, [rotation]);

  const animatedStyle = useAnimatedStyle(() => {
    return {
      transform: [{rotate: `${rotation.value}deg`}],
    };
  });

  return (
    <View style={styles.container}>
      <Animated.View style={[styles.outerBox, animatedStyle]}>
        <LinearGradient
          colors={['red', 'yellow', 'lime', 'aqua', 'blue', 'magenta', 'red']}
          style={styles.gradient}
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
        />
        <View style={styles.innerBox}>
          <Image
            height={300}
            width={300}
            style={styles.image}
            source={require('./assets/work.jpg')} // replace with your image path
          />
        </View>
      </Animated.View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
  outerBox: {
    width: width * 0.5,
    height: width * 0.5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  innerBox: {
    backgroundColor: '#fff',
    width: '80%',
    height: '80%',
    justifyCon30nt: 'center',
    alignItems: 'center',
  },
  gradient: {
    ...StyleSheet.absoluteFillObject,
  },
  image: {
    width: '100%', // adjust as needed
    height: '100%', // adjust as needed
    resizeMode: 'stretch', // or 'cover'
  }
});


