import { Pressable, StyleSheet, Text, View } from "react-native";
import Colors from "../../constants/colors";

function PrimaryButton({ labelStyle, containerStyle, children, onPressHandler = () => { } }) {
    return (
        <View style={[styles.buttonOuterContainer, containerStyle]}>
        <Pressable style={({pressed})=> pressed ? [styles.buttonInnerContainer,styles.pressed] : styles.buttonInnerContainer} 
        onPress={onPressHandler} 
        android_ripple={{ color: Colors.primary600}}>
            
            <Text style={[styles.buttonText, labelStyle]}>
                {children}
            </Text>
            
        </Pressable>
        </View>
    )
}

export default PrimaryButton;

const styles = StyleSheet.create({
    buttonText: {
        color: "white",
        // fontSize: 28,
        textAlign:"center"
    },
    buttonOuterContainer: {
        borderRadius: 28,
        margin:4,
        overflow:"hidden",
        elevation:4
    },
    buttonInnerContainer: {
        backgroundColor: Colors.primary500,
        paddingVertical: 8, 
        paddingHorizontal: 16, 
        
    },
    pressed: {
        opacity: 0.75
    }
})