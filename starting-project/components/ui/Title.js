import { Text, StyleSheet, View } from "react-native";
import Colors from "../../constants/colors";

export default function Title({children}) {
  return (
    <View>
      <Text style={styles.title}>{children}</Text>
    </View>
  )
}


const styles = StyleSheet.create({
    title: {
        fontFamily:"open-sans-bold",
        fontSize: 24,
        // fontWeight: "bold",
        color: "white",
        textAlign: "center",
        borderWidth: 2,
        borderColor:  "white",
        padding: 12,
        width: 300,
        maxWidth: "80%"
    },
});
