import { StyleSheet, Text, View, useWindowDimensions } from 'react-native'
import React from 'react'
import Colors from '../../constants/colors'

export default function Card({children}) {
    const {height, width} = useWindowDimensions();
    return (<View style={[styles.card, {
        paddingVertical: height < 400 ? 4 : 16,
        paddingHorizontal: width < 380 ? 4 : 16,
        marginTop: width < 380 ? 18 : 36,
    }]}>
    {children}
    </View>)
}

const styles = StyleSheet.create({
    card: {
        borderRadius: 8,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: Colors.primary800,
        elevation: 16,
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 1,

    },
})