import { FlatList, StyleSheet, Text, View } from "react-native";
import React from "react";
import GuessLogNumber from "./GuessLogNumber";

export default function LogComponent({ log }) {
  
    return (
        <View style={styles.container}>
            <FlatList
                data={log}
                renderItem={(itemData) => (
                    <GuessLogNumber
                        index={itemData.index}
                        number={itemData.item}
                    />
                )}
            ></FlatList>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {flex: 1, padding: 16 },
});
