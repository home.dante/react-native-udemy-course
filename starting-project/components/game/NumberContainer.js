import { StyleSheet, Text, View, useWindowDimensions} from 'react-native'
import React from 'react'
import Colors from '../../constants/colors'

export default function NumberContainer({children}) {
  const { width, height } = useWindowDimensions();
  return (
    <View style={[styles.container,
    {
      paddingHorizontal: width < 380 ? 12 :   24,
      marginHorizontal: width < 380 ? 12 :   24,
      paddingVertical: width > 600 ? 0 :   24,
      marginVertical: width > 600 ? 0 :   24,
    }
    ]}>
      <Text style={[styles.numberText, {
        fontSize: width < 300 ? 12 : 36,
      }]}>{children}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
    container: {
        borderWidth:4,
        borderColor: Colors.accent500,
        borderRadius: 8,
        alignItems: 'center',
        justifyContent:'center'
    },
    numberText:{
        color: Colors.accent500,
        fontFamily:"open-sans-bold"
        // fontWeight:"bold"
    }
})