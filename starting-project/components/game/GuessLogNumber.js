import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Colors from '../../constants/colors';

export default function GuessLogNumber({ number, index }) {
    return (
        <View key={index} style={styles.listItem}>
            <Text style={styles.guessLabel}>
                #{index + 1} 
            </Text>
            <Text style={styles.guessLabel}>
            Opponent's Guess: {number}
            </Text>
        </View>
    );
}

const styles = StyleSheet.create({
    guessLabel: {
        color: "black",
        fontFamily: "open-sans",
        fontSize: 18,
        textAlign: "center",
    },
    listItem: {
        borderColor: Colors.primary800,
        borderWidth: 1,
        borderRadius: 40,
        padding: 12,
        marginVertical: 8,
        backgroundColor:Colors.accent500,
        flexDirection: "row",
        justifyContent: "space-between",
        width: '100%',
        elevation: 4,
        shadowColor:"black",
        shadowOffset: {width: 0, height: 0},
        shadowOpacity: 0.25,
        shadowRadius: 3
    }
})


