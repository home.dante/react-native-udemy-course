import { StatusBar } from "expo-status-bar";
import { useEffect, useState } from "react";
import {
    ImageBackground,
    SafeAreaView,
    StyleSheet,
    Text,
    View,
    useWindowDimensions
} from "react-native";
import StartGameScreen from "./screens/StartGameScreen";
import { LinearGradient } from "expo-linear-gradient";
import GameScreen from "./screens/GameScreen";
import GameOverScreen from "./screens/GameOverScreen";
import { useFonts } from "expo-font";
import * as SplashScreen from 'expo-splash-screen';
SplashScreen.preventAutoHideAsync();

export default function App() {
    const [userNumber, setUserNumber] = useState();
    const [gameIsOver, setGameIsOver] = useState(true);
    const [rounds, setRounds] = useState(0);
    const [log, setLog] = useState([]);

    let {width, height} = useWindowDimensions();

    const [fontsLoaded, fontError] = useFonts({
      'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
      'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf')
    });

    if(fontsLoaded) {
        SplashScreen.hideAsync();
    }

    const onGameIsOverHandler = () => {
        setGameIsOver(true);
    };

    const onNumberChosenHandler = (number) => {
        setLog((log)=>[...log, number]);
        setRounds((rounds)=>(rounds+1));
    }

    function pickedNumberHandler(pickedNumber) {
        setUserNumber(pickedNumber);
        setGameIsOver(false);
    }

    let screen = <StartGameScreen pickNumberHandler={pickedNumberHandler} />;

    const startNewGameHandler = () => {
        setUserNumber(undefined);
        setRounds(0);
        setLog([]);
    };

    if (userNumber) {
        screen = (
            <GameScreen
                log={log}
                onNumberChosenHandler={onNumberChosenHandler}
                onGameIsOver={onGameIsOverHandler}
                userNumber={userNumber}
            />
        );
    }

    if (gameIsOver && userNumber) {
        screen = <GameOverScreen log={log} roundsNumber={rounds} userNumber={userNumber} startNewGameHandler={startNewGameHandler} />;
    }

    return (
        <>
        {/* <StatusBar translucent={true} style="light" hidden={false}></StatusBar> */}
            <LinearGradient
                colors={["#4e0329", "#ddb52f"]}
                style={{
                    height, width
                }}
            >
                <ImageBackground
                    imageStyle={styles.backgroundImage}
                    resizeMode="cover"
                    source={require("./assets/background.png")}
                    style={{
                        height, width
                        
                    }}
                >
                    <SafeAreaView 
                     style={{
                        height, width
                    }}
                    >
                        {screen}
                    </SafeAreaView>
                   
                </ImageBackground>
            </LinearGradient>
        </>
    );
}

const styles = StyleSheet.create({
    rootScreen: {
},
    backgroundImage: {
        opacity: 0.15,
    },
    abs: { 
    }
});
