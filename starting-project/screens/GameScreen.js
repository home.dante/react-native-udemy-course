import { Alert, StyleSheet, Text, View, Dimensions, useWindowDimensions } from "react-native";
import Title from "../components/ui/Title";
import generateRandomBetween from "../extra-files/logic/random";
import { useEffect, useMemo, useState } from "react";
import NumberContainer from "../components/game/NumberContainer";
import PrimaryButton from "../components/ui/PrimaryButton";
import InstructionText from "../components/ui/InstructionText";
import Card from "../components/ui/Card";
import { Ionicons } from "@expo/vector-icons";
import LogComponent from "../components/game/LogComponent";
let minBoundary = 1;
let maxBoundary = 100;
const useDynamicStyles = () => {
    const layout = useWindowDimensions();
  
    return StyleSheet.create({
      rootContainer: {
        flex: 1,
        alignItems: "center",
        marginTop: layout.height < 400 ? 30 : 100,
        padding: layout.width < 400 ? 8 : 32,
      },
      screen: {
        flex: 1,
        padding: layout.height < 400 ? 12 : 24,
        alignItems:"center",
    }
    });
  };
function GameScreen({
    log,
    userNumber,
    onGameIsOver = () => {},
    onNumberChosenHandler = (value) => {},
}) {
    const initialGuess = generateRandomBetween(1, 100, userNumber);
    const [currentGuess, setCurrentGuess] = useState(initialGuess);
    const dynamicStyles = useDynamicStyles();
    const {width, height} = useWindowDimensions();

    useEffect(() => {
        minBoundary = 1;
        maxBoundary = 100;
    }, []);

    useEffect(() => {
        if (currentGuess !== undefined) {
            onNumberChosenHandler(currentGuess);
        }
    }, [currentGuess]);

    const guessLowerHandler = () => {
        nextGuessHandler("lower");
    };
    const guessHigherHandler = () => {
        nextGuessHandler("higher");
    };

    useEffect(() => {
        if (currentGuess === userNumber) {
            Alert.alert("Your number is", currentGuess + " !", [
                { text: "OK!", style: "cancel" },
            ]);
            onGameIsOver();
        }
    }, [currentGuess]);

    const nextGuessHandler = (direction) => {
        if (
            (direction === "lower" && currentGuess < userNumber) ||
            (direction === "higher" && currentGuess > userNumber)
        ) {
            Alert.alert("Don't lie", "You give false hints", [
                { text: "Sorry!", style: "cancel" },
            ]);
            return;
        }

        if (direction === "lower") {
            maxBoundary = currentGuess;
        } else {
            minBoundary = currentGuess + 1;
        }
        const newRndNumber = generateRandomBetween(
            minBoundary,
            maxBoundary,
            currentGuess
        );
        setCurrentGuess(() => newRndNumber);
    };

    let content = (<> 
    <NumberContainer>{currentGuess}</NumberContainer>
    <Card>
        <InstructionText style={styles.instructionsText}>
            Higher or lower?
        </InstructionText>
        <View style={styles.controls}>
            <View style={styles.buttonContainer}>
                <PrimaryButton onPressHandler={guessHigherHandler}>
                    <Ionicons
                        size={24}
                        color="white"
                        name="md-add"
                    />
                </PrimaryButton>
            </View>
            <View style={styles.buttonContainer}>
                <PrimaryButton onPressHandler={guessLowerHandler}>
                    <Ionicons
                        size={24}
                        color="white"
                        name="md-remove"
                    />
                </PrimaryButton>
            </View>
        </View>
    </Card></>);

    if(width > 600 ) {
        content = 
        <>
        <InstructionText style={[styles.instructionsText, {marginTop:10}]}>
            Higher or lower?
        </InstructionText>
        <View style={styles.buttonsContainerWide}>
        <View style={styles.buttonContainer}>
                <PrimaryButton onPressHandler={guessHigherHandler}>
                    <Ionicons
                        size={24}
                        color="white"
                        name="md-add"
                    />
                </PrimaryButton>
            </View>
        <NumberContainer>{currentGuess}</NumberContainer>

            <View style={styles.buttonContainer}>
                <PrimaryButton onPressHandler={guessLowerHandler}>
                    <Ionicons
                        size={24}
                        color="white"
                        name="md-remove"
                    />
                </PrimaryButton>
            </View>
        </View>
        </>
    }

    return (
        <View style={dynamicStyles.screen}>
                <Title>Opponent's guess</Title>
               {content}
            <LogComponent log={log} />
        </View>
    );
}

export default GameScreen;


const styles = StyleSheet.create({
    // rootContainer: {
    //     flex: 1,
    //     marginTop: deviceHeight < 400 ? 0 : 100,
    // },
    // screen: {
    //     flex: 1,
    //     padding: deviceHeight < 400 ? 12 : 24,
    //     alignItems:"center",
    // },
    instructionsText: {
        marginBottom: 12,
    },
    controls: {
        flexDirection: "row",
    },
    buttonContainer: {
        flex: 1,
    },
    buttonsContainerWide: {
        flexDirection:"row",
        alignItems:"center"
    }
});
