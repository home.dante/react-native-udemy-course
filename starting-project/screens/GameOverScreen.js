import { Image, StyleSheet, Text, View , Dimensions, ScrollView, useWindowDimensions} from "react-native";
import React from "react";
import PrimaryButton from "../components/ui/PrimaryButton";
import Title from "../components/ui/Title";
import Colors from "../constants/colors";

const useDynamicStyles = () => {
    const layout = useWindowDimensions();
  
    return StyleSheet.create({
        imageContainer: {
            width: layout.width < 300 ? 150 : 300,
            height: layout.width < 300 ? 150 : 300,
            borderRadius: layout.width < 300 ? 75 : 150,
            overflow: "hidden",
            borderWidth: 3,
            borderColor: Colors.primary800,
            margin: 36,
        }
    });
  };

export default function GameOverScreen({
    startNewGameHandler,
    roundsNumber = "-",
    userNumber = "-"
}) {

    const dynamicStyles = useDynamicStyles();

    return (
        <ScrollView contentContainerStyle={{alignItems: "center", padding: 50}} style={styles.rootContainer}>
            <Title>Game Over!</Title>
            <View style={dynamicStyles.imageContainer}>
                <Image
                    style={{ width: "100%", height: "100%" }}
                    source={require("../assets/success.png")}
                />
            </View>
            <Text style={styles.summaryText}>
                Your phone needed{" "}
                <Text style={styles.highlight}>{roundsNumber}</Text> rounds to
                guess the number{" "}
                <Text style={styles.highlight}>{userNumber}</Text>.
            </Text>
            <PrimaryButton
                labelStyle={{ padding: 5, fontSize: 24 }}
                onPressHandler={startNewGameHandler}
            >
                Start New Game
            </PrimaryButton>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    rootContainer: {
        flex: 1,
        padding: 24,
    },
    summaryText: {
        fontFamily: "open-sans",
        fontSize: 24,
        textAlign: "center",
        marginBottom: 24,
    },
    highlight: {
        fontFamily: "open-sans-bold",
        color: Colors.primary500,
    }
});
