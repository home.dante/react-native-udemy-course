import { Alert, StyleSheet, Text, TextInput, View, useWindowDimensions, KeyboardAvoidingView, ScrollView } from 'react-native';
import PrimaryButton from '../components/ui/PrimaryButton';
import React, { useEffect, useState } from 'react';
import Colors from '../constants/colors';
import Title from '../components/ui/Title';
import Card from '../components/ui/Card';
import InstructionText from '../components/ui/InstructionText';
import { SceneMap, TabBar, TabView } from 'react-native-tab-view';
import { Badge, Button, TouchableRipple } from "react-native-paper";
import { Keyboard, Platform } from 'react-native';
// Custom hook to calculate styles based on window dimensions
const useDynamicStyles = () => {
    const layout = useWindowDimensions();
  
    return StyleSheet.create({
      rootContainer: {
        flex: 1,
        alignItems: "center",
        marginTop: layout.height < 400 ? 30 : 100,
        padding: layout.width < 400 ? 8 : 32,
      },
    });
  };
const FirstRoute = () => (
    <View style={{ flex: 1, backgroundColor: '#ff4081' }} />
  );
  
  const SecondRoute = () => (
    <View style={{ flex: 1, backgroundColor: '#673ab7' }} />
  );  
  const ThirdRoute = () => (
    <View style={{ flex: 1, backgroundColor: '#673ab7' }} />
  );
  
  const renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
    third: ThirdRoute
  });
function StartGameScreen({pickNumberHandler}) {
    const [index, setIndex] = React.useState(0);
    const tabs = [];
    const dynamicStyles = useDynamicStyles(); 
    const [keyboardOffset, setKeyboardOffset] = useState(0);
    const { width, height } = useWindowDimensions();
    useEffect(()=> {
    const keyboardDidShowListener = Keyboard.addListener(
        Platform.OS === 'android' ? 'keyboardDidShow' : 'keyboardWillShow',
        (event) => {
          console.log("event", event)
          setKeyboardOffset(()=>event.endCoordinates.height);
        }
      );
      const keyboardDidHideListener = Keyboard.addListener(
        Platform.OS === 'android' ? 'keyboardDidHide' : 'keyboardWillHide',
        (event) => {
          console.log("event", event)
          setKeyboardOffset(0);
        }
      );
        // Remove listeners when component unmounts
  return () => {
    keyboardDidShowListener.remove();
    keyboardDidHideListener.remove();
  };
}, []);
    const [routes] = React.useState([
      { key: 'first', title: 'First' },
      { key: 'second', title: 'Second' },
      { key: 'third', title: 'Third' },
    ]);

    const [enteredNumber, setEnteredNumber] = useState('');

    const renderTabBar = props => (
        <TabBar
          {...props}
          indicatorStyle={{ backgroundColor: 'white' }}
          style={{ backgroundColor: 'pink' }}
        />
      );
    function numberInputHandler(enteredText) {
        setEnteredNumber(enteredText);
        
    }

    function resetInputHandler() {
        setEnteredNumber('')
    }

    function confirmInputHandler () {
        const chosenNumber = parseInt(enteredNumber);
        if(isNaN(chosenNumber) || chosenNumber <=0  || chosenNumber  > 99) {
            Alert.alert(
                'Invalid Number', 
                "Number has to be a number between 1 and 99.",
                [
                // {text: "Cancel", style: 'cancel', onPress:()=>{console.log("canceled")}},
                {text: "Cancel", style: 'destructive', onPress: ()=>{}},
                {text: "Okay", style: 'default', onPress: resetInputHandler}
            ]
                )
            return;
        }
        else {
            pickNumberHandler(chosenNumber);
        }
    }

    const marginTopDistance = height < 380 ? 30 : 100;

    return (
      // <ScrollView style={styles.screen}>
      //   <KeyboardAvoidingView 
      //   behavior='padding' 
      //   style={styles.screen}
      //   // keyboardVerticalOffset={keyboardOffset}
      //   >
        
        <View style={[dynamicStyles.rootContainer,  
        {
           marginTop: marginTopDistance,
          //  marginTop: height < 400 ? 30 : 100,
           padding: width < 400 ? 8 : 32,
           }]}>
        
            <Title>
                GUESS MY NUMBER
            </Title>
            <Card>
            <InstructionText>
            Enter a number
            </InstructionText>
                <TextInput 
                disableFullscreenUI={true}
                keyboardType="numeric"
                maxLength={2}
                autoCapitalize='none' 
                autoCorrect={false} 
                style={styles.textInput}
                value={enteredNumber}
                onChangeText={numberInputHandler} 
                />
                <View style={styles.buttonsContainer}>
                    <View style={styles.buttonContainer}>
                        <PrimaryButton onPressHandler={resetInputHandler} >Reset</PrimaryButton>
                    </View>
                    <View style={styles.buttonContainer}>
                        <PrimaryButton onPressHandler={confirmInputHandler} >Confirm</PrimaryButton>
                    </View>
                </View>
                
                {/* <TabView
                 renderTabBar={renderTabBar}
                 lazy={true}
      navigationState={{ index, routes }}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={{ width: layout.width }}
    /> */}

    </Card>
            
        </View>

        //  </KeyboardAvoidingView>

        //  </ScrollView>
    );
}

export default StartGameScreen;

const styles = StyleSheet.create({
    rootContainer: {
        flex:1,
        alignItems:"center",
    },
    screen:{
        flex:1,
    },
    textInput: {
        width: 50,
        textAlign: "center",
        height: 50,
        fontSize: 32,
        borderBottomColor: Colors.accent500,
        borderBottomWidth: 2,
        color: Colors.accent500,
        marginVertical: 8,
        fontWeight: "bold"

    },
    buttonsContainer: {
        flexDirection: "row",
    },
    buttonContainer: {
        flex: 1
    }
});